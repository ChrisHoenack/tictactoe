import React from 'react';
import ReactDOM from 'react-dom';

import { App } from './components/app';

// Add CSS files to bundle
require('../src/css/application.scss');

const state = {
    cells: [
        {
            id: 1,
            checkbox: "empty"
        },
        {
            id: 2,
            checkbox: "empty"
        },
        {
            id: 3,
            checkbox: "empty"
        },
        {
            id: 4,
            checkbox: "empty"
        },
        {
            id: 5,
            checkbox: "empty"
        },
        {
            id: 6,
            checkbox: "empty"
        },
        {
            id: 7,
            checkbox: "empty"
        },
        {
            id: 8,
            checkbox: "empty"
        },
        {
            id: 9,
            checkbox: "empty"
        }
        ]
}

// Render application to DOM
ReactDOM.render(
    <App name="Tic Tac Toe" state={state} />,
    document.getElementById('app')
);