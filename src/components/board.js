import React from 'react';

import { Cell } from './cell';

export class Board extends React.Component {
    render() {
        return (
            <div className="board">
                <div className="row">
                <Cell cellId={ this.props.cells[0].id } checkbox={ this.props.cells[0].checkbox }/>
                <Cell cellId={ this.props.cells[1].id } checkbox={ this.props.cells[1].checkbox }/>
                <Cell cellId={ this.props.cells[2].id } checkbox={ this.props.cells[2].checkbox }/>
                </div>
                <div className="row">
                <Cell cellId={ this.props.cells[3].id } checkbox={ this.props.cells[3].checkbox }/>
                <Cell cellId={ this.props.cells[4].id } checkbox={ this.props.cells[4].checkbox }/>
                <Cell cellId={ this.props.cells[5].id } checkbox={ this.props.cells[5].checkbox }/>
                </div>
                <div className="row">
                <Cell cellId={ this.props.cells[6].id } checkbox={ this.props.cells[6].checkbox }/>
                <Cell cellId={ this.props.cells[7].id } checkbox={ this.props.cells[7].checkbox }/>
                <Cell cellId={ this.props.cells[8].id } checkbox={ this.props.cells[8].checkbox }/>
                </div>
            </div>
        );
    }
};