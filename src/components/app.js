import React from 'react';

import { Board } from './board';

export class App extends React.Component {
    render() {
        return (
            <div className="app">
                <h1 className="title">Tic Tac Toe</h1>
                <Board cells={ this.props.state.cells }/>
            </div>
        );
    }
};