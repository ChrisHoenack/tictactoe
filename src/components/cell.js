import React from 'react';

export class Cell extends React.Component {
    constructor(props) {
        super(props);
        this.state = { checkbox: this.props.checkbox };
    }
    render() {
        let nextState = "";
        if (this.state.checkbox == "empty") {
            nextState = "checkx";
        } else if (this.state.checkbox == "checkx") {
            nextState = "checko";
        } else if (this.state.checkbox == "checko") {
            nextState = "checkx";
        }
        return (
            <div className={ this.state.checkbox + " cell"}
                 onClick={ () => this.setState({ checkbox: nextState })}>
            </div>
        );
    }
};